package com.imbytech.hydrate;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;

public abstract class JsonPrimitiveDecoder<T> extends Decoder<T> {
    public T decode(JsonElement element) {
        if(element != null && element.isJsonPrimitive()) {
            return decodePrimitive(element.getAsJsonPrimitive());
        } else {
            return null;
        }
    }

    public abstract T decodePrimitive(JsonPrimitive primitive);
}