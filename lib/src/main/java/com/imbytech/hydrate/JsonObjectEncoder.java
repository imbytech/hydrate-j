package com.imbytech.hydrate;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonNull;

public abstract class JsonObjectEncoder<T> extends Encoder<T> {
    public JsonElement encode(T input) {
        if(input == null) {
            return JsonNull.INSTANCE;
        } else {
            return encodeObject(input);
        }
    }

    public abstract JsonObject encodeObject(T input);
}