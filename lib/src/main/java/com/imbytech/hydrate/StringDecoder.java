package com.imbytech.hydrate;

import com.google.gson.JsonPrimitive;

public class StringDecoder extends JsonPrimitiveDecoder<String> {
    private boolean strict;

    public StringDecoder(boolean strict) {
        this.strict = strict;
    }

    public StringDecoder() {
        this(false);
    }

    public String decodePrimitive(JsonPrimitive primitive) {
        if(!strict || primitive.isString()) {
            return primitive.getAsString();
        } else {
            return null;
        }
    }
}
