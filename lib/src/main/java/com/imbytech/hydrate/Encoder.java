package com.imbytech.hydrate;

import java.util.function.Function;
import com.google.gson.JsonElement;

/**
 * <p>A JSON encoder. It consumes an object of type {@code <T>} and produces
 * a JSON element. An Encoder implements the
 * {@link Function} interface so can be used in place of functions
 * if required.</p>
 *
 * @param <T> the type of objects consumed by this decoder
 */
public abstract class Encoder<T> implements Function<T,JsonElement> {
    /**
     * Create a new Encoder.
     */
    protected Encoder() {
        // do nothing
    }
    
    /**
     * Encode an object of type {@code <T>} to a JSON element.
     *
     * @param input the object to encode
     * @return the encoded JSON element
     */
    public abstract JsonElement encode(T input);

    /**
     * Implements the {@link Function} API, which means that an encoder
     * can be used in any place where a function that takes a {@code <T>}
     * parameter and returns a value of type {@link JsonElement} is required.
     *
     * @param input the object to encode
     * @return the encoded JSON element
     */
    public JsonElement apply(T input) {
        return encode(input);
    }

    /**
     * Creates a derived encoder that consumes objects of type {@code <V>}
     * by mapping its input to type {@code <T>} through a
     * mapping function.
     * <p>
     * As an example, an encoder that encodes the length of a string could be
     * instantiated as follows:
     * <pre>
     * new NumberEncoder().map(s -> s.length())
     * </pre>
     *
     * @param <V> the type of input of the mapper function, and consumed by the new encoder
     * @param mapper the mapping function to apply to the input
     * @return an encoder that consumes objects of type {@code <V>} rather
     *     than {@code <T>}
     */
    public <V> Encoder<V> map(Function<? super V,? extends T> mapper) {
        return new MappingEncoder<T,V>(this, mapper);
    }

    /**
     * Creates a derived encoder that applies a default value when the main
     * encoder is given a {@code null} input.
     * <p>
     * As an example, a string encoder that ensures no {@code null} values are
     * encoded could be instantiated as follows:
     * <pre>
     * new StringEncoder().withDefault("")
     * </pre>
     *
     * @param defaultValue the default value to apply whe the input is {@code null}
     * @return an encoder that consumes objects of type {@code <T>}
     */
    public Encoder<T> withDefault(T defaultValue) {
        return new WithDefaultEncoder<T>(this, defaultValue);
    }

    private static class MappingEncoder<T,V> extends Encoder<V> {
        private Encoder<T> encoder;
        private Function<? super V,? extends T> mapper;

        public MappingEncoder(Encoder<T> encoder, Function<? super V,? extends T> mapper) {
            this.encoder = encoder;
            this.mapper = mapper;
        }

        public JsonElement encode(V element) {
            T mapped = mapper.apply(element);
            return encoder.encode(mapped);
        }
    }

    private static class WithDefaultEncoder<T> extends Encoder<T> {
        private Encoder<T> encoder;
        private T defaultValue;

        public WithDefaultEncoder(Encoder<T> encoder, T defaultValue) {
            this.encoder = encoder;
            this.defaultValue = defaultValue;
        }

        public JsonElement encode(T element) {
            if(element == null) {
                return encoder.encode(defaultValue);
            } else {
                return encoder.encode(element);
            }
        }
    }
}
