package com.imbytech.hydrate;

import java.util.function.Function;
import com.google.gson.JsonElement;

/**
 * <p>A JSON decoder. It produces an object of type {@code <T>} or {@code null}
 * if it cannot parse the given JSON element. A Decoder implements the
 * {@link Function} interface so can be used in place of functions
 * if required.</p>
 *
 * @param <T> the type of objects produced by this decoder
 */
public abstract class Decoder<T> implements Function<JsonElement,T> {
    /**
     * Create a new Decoder.
     */
    protected Decoder() {
        // do nothing
    }
    
    /**
     * Decode a JSON element to an object of type {@code <T>} or {@code null} if
     * it fails to decode.
     *
     * @param element the JSON element to decode
     * @return the decoded object
     */
    public abstract T decode(JsonElement element);

    /**
     * Implements the {@link Function} API, which means that a decoder
     * can be used in any place where a function that takes a {@link JsonElement}
     * parameter and returns a value of type {@code <T>} is required.
     *
     * @param element the JSON element to decode
     * @return the decoded object
     */
    public T apply(JsonElement element) {
        return decode(element);
    }

    /**
     * Creates a derived decoder that returns objects of type {@code <V>}
     * by mapping the result of a type {@code <T>} decoder through a
     * mapping function. If the result is {@code null},
     * the mapping will not be applied, therefore the mapping function can
     * assume that its parameter is not {@code null}.
     * <p>
     * As an example, a decoder that decodes the length of a string could be
     * instantiated as follows:
     * <pre>
     * new StringDecoder().map(s -> s.length())
     * </pre>
     *
     * @param <V> the type of output of the mapper function, and produced by the new decoder
     * @param mapper the mapping function to apply to the result
     * @return a decoder that returns objects of type {@code <V>} rather
     *     than {@code <T>}
     */
    public <V> Decoder<V> map(Function<? super T,? extends V> mapper) {
        return new MappingDecoder<T,V>(this, mapper);
    }

    /**
     * Creates a derived decoder that applies a default value when the main
     * decoder returns {@code null}.
     * <p>
     * As an example, a string decoder that ensures no {@code null} values are
     * returned could be instantiated as follows:
     * <pre>
     * new StringDecoder().withDefault("")
     * </pre>
     *
     * @param defaultValue the default value to apply when the decoder returns {@code null}
     * @return a decoder that returns objects of type {@code <T>} that are
     *     never {@code null}
     */
    public Decoder<T> withDefault(T defaultValue) {
        return new WithDefaultDecoder<T>(this, defaultValue);
    }

    private static class MappingDecoder<T,V> extends Decoder<V> {
        private Decoder<T> decoder;
        private Function<? super T,? extends V> mapper;

        public MappingDecoder(Decoder<T> decoder, Function<? super T,? extends V> mapper) {
            this.decoder = decoder;
            this.mapper = mapper;
        }

        public V decode(JsonElement element) {
            T result = decoder.decode(element);
            if(result != null) {
                return mapper.apply(result);
            } else {
                return null;
            }
        }
    }

    private static class WithDefaultDecoder<T> extends Decoder<T> {
        private Decoder<T> decoder;
        private T defaultValue;

        public WithDefaultDecoder(Decoder<T> decoder, T defaultValue) {
            this.decoder = decoder;
            this.defaultValue = defaultValue;
        }

        public T decode(JsonElement element) {
            T result = decoder.decode(element);
            if(result != null) {
                return result;
            } else {
                return this.defaultValue;
            }
        }
    }
}
