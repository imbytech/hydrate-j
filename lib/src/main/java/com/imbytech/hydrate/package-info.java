/**
 * <p>Set of classes to encode and decode data to and from JSON using the
 * Google Gson parser in a way that is type safe.</p>
 *
 * <p>The design of the package is heavily inspired from the Elm JSON decoders.
 * The idea is to be able to create decoder and encoder objects that can be
 * re-used and combined to handle complex data structures.</p>
 *
 * <h2>Decoders</h2>
 *
 * <p>All decoders are sub-classes of abstract class
 * {@link com.imbytech.hydrate.Decoder}. They are
 * grouped into four main categories:</p>
 *
 * <ul>
 *   <li>Decoders that operate on JSON primitives:
 *   {@link com.imbytech.hydrate.BooleanDecoder},
 *   {@link com.imbytech.hydrate.StringDecoder},
 *   {@link com.imbytech.hydrate.NumberDecoder} and
 *   {@link com.imbytech.hydrate.EnumDecoder};</li>
 *   <li>Decoders that operate on JSON arrays: {@link com.imbytech.hydrate.StreamDecoder};</li>
 *   <li>Decoders that operate on JSON objects: {@link com.imbytech.hydrate.ObjectDecoder},
 *   {@link com.imbytech.hydrate.MapDecoder} and
 *   {@link com.imbytech.hydrate.FieldDecoder};</li>
 *   <li>Decision tree decoders to handle variable structures:
 *   {@link com.imbytech.hydrate.OneOfDecoder}.</li>
 * </ul>
 *
 * <p>Decoders that handle data types other than the ones handled by the basic
 * decoders can be created using the {@link com.imbytech.hydrate.Decoder#map} and
 * {@link com.imbytech.hydrate.Decoder#withDefault} functions.</p>
 *
 * <p>In addition, the {@link com.imbytech.hydrate.Decoders}
 * class offers a number of static methods
 * to create standard derived decoders such as a number or list decoders.</p>
 *
 * <h2>Encoders</h2>
 *
 * <p>All encoders are sub-classes of abstract class
 * {@link com.imbytech.hydrate.Encoder}. They are
 * grouped into three main categories:</p>
 *
 * <ul>
 *   <li>Encoders that produce JSON primitives:
 *   {@link com.imbytech.hydrate.BooleanEncoder},
 *   {@link com.imbytech.hydrate.StringEncoder},
 *   {@link com.imbytech.hydrate.NumberEncoder} and
 *   {@link com.imbytech.hydrate.EnumEncoder};</li>
 *   <li>Encoders that produce JSON arrays: {@link com.imbytech.hydrate.StreamEncoder};</li>
 *   <li>Encoders that produce JSON objects: {@link com.imbytech.hydrate.ObjectEncoder} and
 *   {@link com.imbytech.hydrate.MapEncoder};</li>
 * </ul>
 *
 * <p>Encoders that handle data types other than the ones handled by the basic
 * decoders can be created using the {@link com.imbytech.hydrate.Encoder#map} and
 * {@link com.imbytech.hydrate.Encoder#withDefault} functions.</p>
 *
 * <p>In addition, the {@link com.imbytech.hydrate.Encoders}
 # class offers a number of static methods
 * to create standard derived encoders such as a collection decoder.</p>
 */
package com.imbytech.hydrate;