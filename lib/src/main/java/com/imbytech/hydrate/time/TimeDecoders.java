package com.imbytech.hydrate.time;

import java.util.function.Function;
import java.time.ZoneId;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.Period;
import java.time.Duration;
import java.time.DayOfWeek;
import java.time.Month;
import java.time.temporal.TemporalAmount;
import java.time.DateTimeException;
import java.time.format.DateTimeParseException;
import com.imbytech.hydrate.Decoder;
import com.imbytech.hydrate.StringDecoder;
import com.imbytech.hydrate.OneOfDecoder;
import com.imbytech.hydrate.EnumDecoder;
import static com.imbytech.hydrate.Decoders.longDecoder;

/**
 * Set of decoders to handle date and time values stored in JSON as numbers
 * or ISO-8601 strings.
 */
public class TimeDecoders {
    /**
     * Create a JSON decoder that decodes a string as a time zone value.
     *
     * @return a decoder that can decode a time zone from a JSON string
     */
    public static Decoder<ZoneId> zoneIdDecoder() {
        return zoneIdDecoder(null);
    }

    /**
     * Create a JSON decoder that decodes a string as a time zone value, given
     * a default time zone string.
     *
     * @param defaultZone the default time zone string
     * @return a decoder that can decode a time zone from a JSON string
     */
    public static Decoder<ZoneId> zoneIdDecoder(String defaultZone) {
        return new StringDecoder().withDefault(defaultZone).map(
            v -> parseDateTimeObject(w -> ZoneId.of(w), v)
            );
    }

    /**
     * Create a JSON decoder that decodes a number as an instant, given an
     * instant precision.
     *
     * @param precision the instant precision to use
     * @return a decoder that can decode an instant from a JSON number
     */
    public static Decoder<Instant> instantDecoder(InstantPrecision precision) {
        return longDecoder().map(v -> precision.ofEpoch(v));
    }

    /**
     * Create a JSON decoder that decodes a string as a local date.
     *
     * @return a decoder that can decode a local date from a JSON string
     */
    public static Decoder<LocalDate> localDateDecoder() {
        return new StringDecoder().map(v -> parseDateTimeObject(w -> LocalDate.parse(w), v));
    }

    /**
     * Create a JSON decoder that decodes a string as a local date/time.
     *
     * @return a decoder that can decode a local date/time from a JSON string
     */
    public static Decoder<LocalDateTime> localDateTimeDecoder() {
        return new StringDecoder().map(v -> parseDateTimeObject(w -> LocalDateTime.parse(w), v));
    }

    /**
     * Create a JSON decoder that decodes a string as a zoned date/time.
     *
     * @return a decoder that can decode a zoned date/time from a JSON string
     */
    public static Decoder<ZonedDateTime> zonedDateTimeDecoder() {
        return new StringDecoder().map(v -> parseDateTimeObject(w -> ZonedDateTime.parse(w), v));
    }

    /**
     * Create a JSON decoder that decodes a string as a time period.
     *
     * @return a decoder that can decode a time period from a JSON string
     */
    public static Decoder<Period> periodDecoder() {
        return new StringDecoder()
            .map(v -> normaliseTemporal(v))
            .map(v -> parseTemporalObject(w -> Period.parse(w), v));
    }

    /**
     * Create a JSON decoder that decodes a string as a duration.
     *
     * @return a decoder that can decode a duration from a JSON string
     */
    public static Decoder<Duration> durationDecoder() {
        return new StringDecoder()
            .map(v -> normaliseTemporal(v))
            .map(v -> parseTemporalObject(w -> Duration.parse(w), v));
    }

    /**
     * Create a JSON decoder that decodes a string as a temporal amount.
     * The decoder will attempt to decode the temporal amount as a period and
     * will fall back on a duration if it can't parse a period.
     *
     * @return a decoder that can decode a temporal amount from a JSON string
     */
    public static Decoder<TemporalAmount> temporalAmountDecoder() {
        return new OneOfDecoder<TemporalAmount>(periodDecoder(), durationDecoder());
    }

    /**
     * Create a JSON decoder that decodes a string as a day of week.
     *
     * @return a decoder that can decode a day of week from a JSON string
     */
    public static Decoder<DayOfWeek> dayOfWeekDecoder() {
        return EnumDecoder.of(DayOfWeek.class);
    }

    /**
     * Create a JSON decoder that decodes a string as a month.
     *
     * @return a decoder that can decode a month from a JSON string
     */
    public static Decoder<Month> monthDecoder() {
        return EnumDecoder.of(Month.class);
    }

    private static <T> T parseDateTimeObject(Function<String,T> fun, String value) {
        T result = null;
        if(value != null) {
            try {
                result = fun.apply(value);
            } catch(DateTimeException ex) {
                // do nothing, return null
            }
        }
        return result;
    }

    private static <T> T parseTemporalObject(Function<String,T> fun, String value) {
        T result = null;
        if(value != null) {
            try {
                result = fun.apply(value);
            } catch(DateTimeParseException ex) {
                // do nothing, return null
            }
        }
        return result;
    }

    private static String normaliseTemporal(String value) {
        if(value.matches("^[pP-].*")) {
            return value;
        } else {
            return "P".concat(value);
        }
    }
}