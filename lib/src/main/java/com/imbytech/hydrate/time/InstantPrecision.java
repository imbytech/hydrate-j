package com.imbytech.hydrate.time;

import java.time.Instant;

/**
 * Enum that specifies the precision of an instant when encoded to or
 * decoded from an epoch value.
 */
public enum InstantPrecision {
    SECONDS,
    MILLISECONDS;

    /**
     * Convert from epoch to instant.
     *
     * @param epochValue the epoch value
     * @return the corresponding instant
     */
    public Instant ofEpoch(long epochValue) {
        if(this == SECONDS) {
            return Instant.ofEpochSecond(epochValue);
        } else {
            return Instant.ofEpochMilli(epochValue);
        }
    }

    /**
     * Convert from instant to epoch.
     *
     * @param instant the instant
     * @return the corresponding epoch value
     */
    public long toEpoch(Instant instant) {
        if(this == SECONDS) {
            return instant.getEpochSecond();
        } else {
            return instant.toEpochMilli();
        }
    }
}
