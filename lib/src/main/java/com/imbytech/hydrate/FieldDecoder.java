package com.imbytech.hydrate;

import java.util.function.Function;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class FieldDecoder<T> extends JsonObjectDecoder<T> {
    private String fieldName;
    private Function<JsonElement, ? extends T> decoder;

    public FieldDecoder(String fieldName, Function<JsonElement,? extends T> decoder) {
        this.fieldName = fieldName;
        this.decoder = decoder;
    }

    public T decodeObject(JsonObject element) {
        return decoder.apply(element.get(fieldName));
    }
}
