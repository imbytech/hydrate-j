package com.imbytech.hydrate;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonNull;

public class NumberEncoder extends JsonPrimitiveEncoder<Number> {
    public JsonElement encode(Number input) {
        if(input == null || Double.isNaN(input.doubleValue())) {
            return JsonNull.INSTANCE;
        } else {
            return encodePrimitive(input);
        }
    }

    public JsonPrimitive encodePrimitive(Number input) {
        return new JsonPrimitive(input);
    }
}
