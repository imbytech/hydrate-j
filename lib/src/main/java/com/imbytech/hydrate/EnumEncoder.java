package com.imbytech.hydrate;

import com.google.gson.JsonPrimitive;

public class EnumEncoder<E extends Enum<E>> extends JsonPrimitiveEncoder<E> {
    private boolean toLowercase;

    public EnumEncoder(boolean  toLowercase) {
        super();
        this.toLowercase = toLowercase;
    }

    public EnumEncoder() {
        this(false);
    }

    public JsonPrimitive encodePrimitive(E input) {
        String value = input.toString();
        if(value != null && toLowercase) {
            value = value.toLowerCase();
        }
        return new JsonPrimitive(value);
    }
}
