package com.imbytech.hydrate;

import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import java.util.function.Function;
import com.google.gson.JsonElement;
import com.google.gson.JsonArray;

public class StreamDecoder<T> extends JsonArrayDecoder<Stream<T>> {
    private Function<JsonElement,T> fun;

    public StreamDecoder(Function<JsonElement,T> fun) {
        this.fun = fun;
    }

    public Stream<T> decodeArray(JsonArray array) {
        return StreamSupport.stream(
            Spliterators.spliterator(
                array.iterator(),
                array.size(),
                Spliterator.IMMUTABLE | Spliterator.SIZED
                ),
            false)
            .map(e -> fun.apply(e))
            .filter(e -> e != null);
    }
}
