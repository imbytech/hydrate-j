package com.imbytech.hydrate;

import com.google.gson.JsonPrimitive;

public class StringEncoder extends JsonPrimitiveEncoder<String> {
    public JsonPrimitive encodePrimitive(String input) {
        return new JsonPrimitive(input);
    }
}
