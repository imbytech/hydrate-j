package com.imbytech.hydrate;

import com.google.gson.JsonPrimitive;

public class NumberDecoder extends JsonPrimitiveDecoder<Number> {
    public Number decodePrimitive(JsonPrimitive primitive) {
        if(primitive.isNumber()) {
            return primitive.getAsNumber();
        } else {
            return null;
        }
    }
}
