package com.imbytech.hydrate;

import com.google.gson.JsonPrimitive;

public class BooleanEncoder extends JsonPrimitiveEncoder<Boolean> {
    public JsonPrimitive encodePrimitive(Boolean input) {
        return new JsonPrimitive(input);
    }
}
