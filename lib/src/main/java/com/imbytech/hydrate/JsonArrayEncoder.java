package com.imbytech.hydrate;

import com.google.gson.JsonElement;
import com.google.gson.JsonArray;
import com.google.gson.JsonNull;

public abstract class JsonArrayEncoder<T> extends Encoder<T> {
    public JsonElement encode(T input) {
        if(input == null) {
            return JsonNull.INSTANCE;
        } else {
            return encodeArray(input);
        }
    }

    public abstract JsonArray encodeArray(T input);
}