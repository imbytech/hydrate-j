package com.imbytech.hydrate;

import java.util.Collection;

public class Encoders {
    public static <T> Encoder<Collection<T>> collectionEncoder(Encoder<T> encoder) {
        return new StreamEncoder<T>(encoder).map(c -> c.stream());
    }
}
