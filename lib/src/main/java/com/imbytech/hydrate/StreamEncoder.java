package com.imbytech.hydrate;

import java.util.stream.Stream;
import java.util.function.Function;
import com.google.gson.JsonElement;
import com.google.gson.JsonArray;

public class StreamEncoder<T> extends JsonArrayEncoder<Stream<T>> {
    private Function<? super T,JsonElement> fun;

    public StreamEncoder(Function<? super T,JsonElement> fun) {
        this.fun = fun;
    }

    public JsonArray encodeArray(Stream<T> input) {
        JsonArray result = new JsonArray();
        input.forEach(v -> result.add(fun.apply(v)));
        return result;
    }
}
