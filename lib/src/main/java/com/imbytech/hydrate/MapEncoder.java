package com.imbytech.hydrate;

import java.util.Map;
import java.util.function.Function;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class MapEncoder<T> extends JsonObjectEncoder<Map<String,T>> {
    private Function<? super T, JsonElement> fun;

    public MapEncoder(Function<? super T, JsonElement> fun) {
        this.fun = fun;
    }

    public JsonObject encodeObject(Map<String,T> input) {
        JsonObject result = new JsonObject();
        for(Map.Entry<String,T> entry : input.entrySet()) {
            result.add(entry.getKey(), fun.apply(entry.getValue()));
        }
        return result;
    }
}