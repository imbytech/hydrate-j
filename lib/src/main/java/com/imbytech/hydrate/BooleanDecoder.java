package com.imbytech.hydrate;

import com.google.gson.JsonPrimitive;

public class BooleanDecoder extends JsonPrimitiveDecoder<Boolean> {
    public Boolean decodePrimitive(JsonPrimitive primitive) {
        if(primitive.isBoolean()) {
            return primitive.getAsBoolean();
        } else {
            return null;
        }
    }
}
