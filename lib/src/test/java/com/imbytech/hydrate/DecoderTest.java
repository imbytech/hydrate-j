package com.imbytech.hydrate;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import com.google.gson.JsonPrimitive;

public class DecoderTest {
    @Test
    public void testMap() {
        JsonPrimitive primitive = new JsonPrimitive(123456L);
        Decoder<Long> decoder = new NumberDecoder().map(v -> v.longValue());
        long result = decoder.decode(primitive);
        assertEquals(123456L, result);
    }

    @Test
    public void testMapWithNull() {
        JsonPrimitive primitive = new JsonPrimitive("I am a string");
        Decoder<Long> decoder = new NumberDecoder().map(v -> v.longValue());
        Long result = decoder.decode(primitive);
        assertNull(result);
    }

    @Test
    public void testWithDefault() {
        JsonPrimitive primitive = new JsonPrimitive("I am a string");
        Decoder<Number> decoder = new NumberDecoder().withDefault(100000L);
        Number result = decoder.decode(primitive);
        assertEquals(100000L, result);
    }

    @Test
    public void testWithDefaultNotTriggered() {
        JsonPrimitive primitive = new JsonPrimitive(123456L);
        Decoder<Number> decoder = new NumberDecoder().withDefault(100000L);
        Number result = decoder.decode(primitive);
        assertEquals(123456L, result);
    }
}
