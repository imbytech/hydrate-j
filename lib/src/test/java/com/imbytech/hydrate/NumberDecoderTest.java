package com.imbytech.hydrate;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import com.google.gson.JsonPrimitive;

public class NumberDecoderTest {
    @Test
    public void testDecodeNumber() {
        JsonPrimitive primitive = new JsonPrimitive(123456L);
        NumberDecoder decoder = new NumberDecoder();
        Number result = decoder.decode(primitive);
        assertEquals(123456L, result);
    }

    @Test
    public void testDecodeString() {
        JsonPrimitive primitive = new JsonPrimitive("I am a string");
        NumberDecoder decoder = new NumberDecoder();
        Number result = decoder.decode(primitive);
        assertNull(result);
    }
}
