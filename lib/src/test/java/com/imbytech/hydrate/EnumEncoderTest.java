package com.imbytech.hydrate;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;

public class EnumEncoderTest {
    @Test
    public void testEncode() {
        EnumEncoder<MyEnum> encoder = new EnumEncoder<MyEnum>();
        JsonElement value = encoder.encode(MyEnum.ONE);
        assertEquals("ONE", value.getAsString());
    }

    @Test
    public void testEncodePrimitive() {
        EnumEncoder<MyEnum> encoder = new EnumEncoder<MyEnum>();
        JsonPrimitive value = encoder.encodePrimitive(MyEnum.ONE);
        assertEquals("ONE", value.getAsString());
    }

    @Test
    public void testEncodeToLowercase() {
        EnumEncoder<MyEnum> encoder = new EnumEncoder<MyEnum>(true);
        JsonElement value = encoder.encode(MyEnum.TWO);
        assertEquals("two", value.getAsString());
    }

    public static enum MyEnum {
        ONE,
        TWO,
        THREE
    }
}
