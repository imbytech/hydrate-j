package com.imbytech.hydrate;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import com.google.gson.JsonElement;

public class StringEncoderTest {
    @Test
    public void testEncodeNumber() {
        StringEncoder encoder = new StringEncoder();
        JsonElement result = encoder.encode("The quick brown fox");
        assertTrue(result.isJsonPrimitive());
        assertEquals("The quick brown fox", result.getAsString());
    }

    @Test
    public void testEncodeNull() {
        StringEncoder encoder = new StringEncoder();
        JsonElement result = encoder.encode(null);
        assertTrue(result.isJsonNull());
    }
}
