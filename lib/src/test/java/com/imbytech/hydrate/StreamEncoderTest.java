package com.imbytech.hydrate;

import java.util.List;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import com.google.gson.JsonArray;

public class StreamEncoderTest {
    @Test
    public void testDecodeStringStream() {
        List<String> input = new ArrayList<String>();
        input.add("hello");
        input.add("world");
        StreamEncoder<String> encoder = new StreamEncoder<String>(new StringEncoder());
        JsonArray result = encoder.encodeArray(input.stream());
        assertEquals(2, result.size());
        assertEquals("hello", result.get(0).getAsString());
        assertEquals("world", result.get(1).getAsString());
    }
}
