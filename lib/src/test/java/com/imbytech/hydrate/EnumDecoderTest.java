package com.imbytech.hydrate;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import com.google.gson.JsonPrimitive;

public class EnumDecoderTest {
    @Test
    public void testOf() {
        EnumDecoder<MyEnum> decoder = EnumDecoder.of(MyEnum.class);
        assertNotNull(decoder);
    }

    @Test
    public void testDecodePrimitive() {
        EnumDecoder<MyEnum> decoder = EnumDecoder.of(MyEnum.class);
        MyEnum value = decoder.decodePrimitive(new JsonPrimitive("ONE"));
        assertEquals(MyEnum.ONE, value);
    }

    @Test
    public void testDecode() {
        EnumDecoder<MyEnum> decoder = EnumDecoder.of(MyEnum.class);
        MyEnum value = decoder.decode(new JsonPrimitive("TWO"));
        assertEquals(MyEnum.TWO, value);
    }

    @Test
    public void testDecodeLowercase() {
        EnumDecoder<MyEnum> decoder = EnumDecoder.of(MyEnum.class);
        MyEnum value = decoder.decode(new JsonPrimitive("three"));
        assertEquals(MyEnum.THREE, value);
    }

    @Test
    public void testDecodeNull() {
        EnumDecoder<MyEnum> decoder = EnumDecoder.of(MyEnum.class);
        MyEnum value = decoder.decode(new JsonPrimitive("invalid"));
        assertNull(value);
    }

    public static enum MyEnum {
        ONE,
        TWO,
        THREE
    }
}
