package com.imbytech.hydrate;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonObject;

public class FieldDecoderTest {
    @Test
    public void testDecodeField() {
        JsonPrimitive primitive = new JsonPrimitive(123456L);
        JsonObject obj = new JsonObject();
        obj.add("number", primitive);
        FieldDecoder<Number> decoder = new FieldDecoder<Number>("number", new NumberDecoder());
        Number result = decoder.decode(obj);
        assertEquals(123456L, result);
    }
}
