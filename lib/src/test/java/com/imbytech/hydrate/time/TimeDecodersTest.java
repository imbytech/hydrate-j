package com.imbytech.hydrate.time;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.Period;
import java.time.Duration;
import java.time.temporal.TemporalAmount;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonObject;
import com.imbytech.hydrate.Decoder;
import com.imbytech.hydrate.FieldDecoder;

public class TimeDecodersTest {

    // ZoneId

    @Test
    public void testZoneId() {
        JsonPrimitive primitive = new JsonPrimitive("Europe/London");
        Decoder<ZoneId> decoder = TimeDecoders.zoneIdDecoder();
        ZoneId result = decoder.decode(primitive);
        assertEquals(ZoneId.of("Europe/London"), result);
    }

    @Test
    public void testZoneIdWithDefaultAndValue() {
        JsonPrimitive primitive = new JsonPrimitive("Europe/Paris");
        JsonObject obj = new JsonObject();
        obj.add("tz", primitive);
        Decoder<ZoneId> decoder = new FieldDecoder<ZoneId>("tz", TimeDecoders.zoneIdDecoder("Europe/London"));
        ZoneId result = decoder.decode(obj);
        assertEquals(ZoneId.of("Europe/Paris"), result);
    }

    @Test
    public void testZoneIdWithDefaultNoValue() {
        JsonObject obj = new JsonObject();
        Decoder<ZoneId> decoder = new FieldDecoder<ZoneId>("tz", TimeDecoders.zoneIdDecoder("Europe/London"));
        ZoneId result = decoder.decode(obj);
        assertEquals(ZoneId.of("Europe/London"), result);
    }

    @Test
    public void testZoneIdNoDefaultNoValue() {
        JsonObject obj = new JsonObject();
        Decoder<ZoneId> decoder = new FieldDecoder<ZoneId>("tz", TimeDecoders.zoneIdDecoder());
        ZoneId result = decoder.decode(obj);
        assertNull(result);
    }

    // Instant

    @Test
    public void testInstantSeconds() {
        JsonPrimitive primitive = new JsonPrimitive(1525129200L);
        Decoder<Instant> decoder = TimeDecoders.instantDecoder(InstantPrecision.SECONDS);
        Instant instant = decoder.decode(primitive);
        ZonedDateTime zdt = instant.atZone(ZoneId.of("Europe/London"));
        assertEquals(2018, zdt.getYear());
        assertEquals(5, zdt.getMonthValue());
        assertEquals(1, zdt.getDayOfMonth());
    }

    @Test
    public void testInstantMilliseconds() {
        JsonPrimitive primitive = new JsonPrimitive(1525129200000L);
        Decoder<Instant> decoder = TimeDecoders.instantDecoder(InstantPrecision.MILLISECONDS);
        Instant instant = decoder.decode(primitive);
        ZonedDateTime zdt = instant.atZone(ZoneId.of("Europe/London"));
        assertEquals(2018, zdt.getYear());
        assertEquals(5, zdt.getMonthValue());
        assertEquals(1, zdt.getDayOfMonth());
    }

    // LocalDate

    @Test
    public void testLocalDate() {
        JsonPrimitive primitive = new JsonPrimitive("2018-05-01");
        Decoder<LocalDate> decoder = TimeDecoders.localDateDecoder();
        LocalDate result = decoder.decode(primitive);
        assertEquals(2018, result.getYear());
        assertEquals(5, result.getMonthValue());
        assertEquals(1, result.getDayOfMonth());
    }

    // LocalDateTime

    @Test
    public void testLocalDateTime() {
        JsonPrimitive primitive = new JsonPrimitive("2018-05-01T10:30");
        Decoder<LocalDateTime> decoder = TimeDecoders.localDateTimeDecoder();
        LocalDateTime result = decoder.decode(primitive);
        assertEquals(2018, result.getYear());
        assertEquals(5, result.getMonthValue());
        assertEquals(1, result.getDayOfMonth());
        assertEquals(10, result.getHour());
        assertEquals(30, result.getMinute());
    }

    // ZonedDateTime

    @Test
    public void testZonedDateTime() {
        JsonPrimitive primitive = new JsonPrimitive("2018-05-01T10:30+01:00");
        Decoder<ZonedDateTime> decoder = TimeDecoders.zonedDateTimeDecoder();
        ZonedDateTime result = decoder.decode(primitive);
        assertEquals(2018, result.getYear());
        assertEquals(5, result.getMonthValue());
        assertEquals(1, result.getDayOfMonth());
        assertEquals(10, result.getHour());
        assertEquals(30, result.getMinute());
        assertEquals(ZoneOffset.ofHours(1), result.getOffset());
    }

    // TemporalAmount

    @Test
    public void testPeriod() {
        JsonPrimitive primitive = new JsonPrimitive("P1D");
        Decoder<Period> decoder = TimeDecoders.periodDecoder();
        Period result = decoder.decode(primitive);
        assertEquals(Period.ofDays(1), result);
    }

    @Test
    public void testPeriodNoPrefix() {
        JsonPrimitive primitive = new JsonPrimitive("1D");
        Decoder<Period> decoder = TimeDecoders.periodDecoder();
        Period result = decoder.decode(primitive);
        assertEquals(Period.ofDays(1), result);
    }

    @Test
    public void testDuration() {
        JsonPrimitive primitive = new JsonPrimitive("PT1H");
        Decoder<Duration> decoder = TimeDecoders.durationDecoder();
        Duration result = decoder.decode(primitive);
        assertEquals(Duration.ofHours(1), result);
    }

    @Test
    public void testDurationNoPrefix() {
        JsonPrimitive primitive = new JsonPrimitive("T1H");
        Decoder<Duration> decoder = TimeDecoders.durationDecoder();
        Duration result = decoder.decode(primitive);
        assertEquals(Duration.ofHours(1), result);
    }

    @Test
    public void testTemporalAmountPeriod() {
        JsonPrimitive primitive = new JsonPrimitive("P1D");
        Decoder<TemporalAmount> decoder = TimeDecoders.temporalAmountDecoder();
        TemporalAmount result = decoder.decode(primitive);
        assertEquals(Period.ofDays(1), result);
    }

    @Test
    public void testTemporalAmountDuration() {
        JsonPrimitive primitive = new JsonPrimitive("PT1H");
        Decoder<TemporalAmount> decoder = TimeDecoders.temporalAmountDecoder();
        TemporalAmount result = decoder.decode(primitive);
        assertEquals(Duration.ofHours(1), result);
    }
}
