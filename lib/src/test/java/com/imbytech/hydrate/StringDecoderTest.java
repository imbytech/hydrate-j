package com.imbytech.hydrate;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import com.google.gson.JsonPrimitive;

public class StringDecoderTest {
    @Test
    public void testDecodeStringLenient() {
        JsonPrimitive primitive = new JsonPrimitive("Hello world!");
        StringDecoder decoder = new StringDecoder();
        String result = decoder.decode(primitive);
        assertEquals("Hello world!", result);
    }

    @Test
    public void testDecodeNumberLenient() {
        JsonPrimitive primitive = new JsonPrimitive(123);
        StringDecoder decoder = new StringDecoder();
        String result = decoder.decode(primitive);
        assertEquals("123", result);
    }

    @Test
    public void testDecodeStringStrict() {
        JsonPrimitive primitive = new JsonPrimitive("Hello world!");
        StringDecoder decoder = new StringDecoder(true);
        String result = decoder.decode(primitive);
        assertEquals("Hello world!", result);
    }

    @Test
    public void testDecodeNumberStrict() {
        JsonPrimitive primitive = new JsonPrimitive(123);
        StringDecoder decoder = new StringDecoder(true);
        String result = decoder.decode(primitive);
        assertNull(result);
    }
}
