package com.imbytech.hydrate;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import com.google.gson.JsonObject;
import com.google.gson.JsonArray;

public class GsonTest {
    @Test
    public void testArrayToString() {
        JsonArray arr = new JsonArray();
        JsonObject obj = new JsonObject();
        obj.addProperty("to", "myself");
        obj.addProperty("title", "hello");
        obj.addProperty("message", "world");
        arr.add(obj);
        assertEquals(
            "[{\"to\":\"myself\",\"title\":\"hello\",\"message\":\"world\"}]",
            arr.toString()
            );
    }
}