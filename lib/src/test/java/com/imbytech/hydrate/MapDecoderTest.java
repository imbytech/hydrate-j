package com.imbytech.hydrate;

import java.util.Map;
import org.junit.jupiter.api.Test;
import com.google.gson.JsonObject;
import static org.hamcrest.MatcherAssert.assertThat;
import org.hamcrest.collection.IsMapContaining;

public class MapDecoderTest {
    @Test
    public void testDecodeStrings() {
        JsonObject obj = new JsonObject();
        obj.addProperty("monday", "lundi");
        obj.addProperty("tuesday", "mardi");
        obj.addProperty("wednesday", "mercredi");
        MapDecoder<String> decoder = new MapDecoder<String>(new StringDecoder());
        Map<String,String> result = decoder.decode(obj);
        assertThat(result, IsMapContaining.hasEntry("monday", "lundi"));
        assertThat(result, IsMapContaining.hasEntry("tuesday", "mardi"));
        assertThat(result, IsMapContaining.hasEntry("wednesday", "mercredi"));
    }

    @Test
    public void testDecodeInts() {
        JsonObject obj = new JsonObject();
        obj.addProperty("monday", 1);
        obj.addProperty("tuesday", 2);
        obj.addProperty("wednesday", 3);
        MapDecoder<Integer> decoder = new MapDecoder<Integer>(Decoders.intDecoder());
        Map<String,Integer> result = decoder.decode(obj);
        assertThat(result, IsMapContaining.hasEntry("monday", 1));
        assertThat(result, IsMapContaining.hasEntry("tuesday", 2));
        assertThat(result, IsMapContaining.hasEntry("wednesday", 3));
    }

    @Test
    public void testDecodeDoubles() {
        JsonObject obj = new JsonObject();
        obj.addProperty("monday", 1.1);
        obj.addProperty("tuesday", 2.2);
        obj.addProperty("wednesday", 3.3);
        MapDecoder<Double> decoder = new MapDecoder<Double>(Decoders.doubleDecoder());
        Map<String,Double> result = decoder.decode(obj);
        assertThat(result, IsMapContaining.hasEntry("monday", 1.1));
        assertThat(result, IsMapContaining.hasEntry("tuesday", 2.2));
        assertThat(result, IsMapContaining.hasEntry("wednesday", 3.3));
    }
}
