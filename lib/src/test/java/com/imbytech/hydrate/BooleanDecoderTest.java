package com.imbytech.hydrate;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import com.google.gson.JsonPrimitive;

public class BooleanDecoderTest {
    @Test
    public void testDecodeBooleanTrue() {
        JsonPrimitive primitive = new JsonPrimitive(true);
        BooleanDecoder decoder = new BooleanDecoder();
        Boolean result = decoder.decode(primitive);
        assertEquals(true, result);
    }

    @Test
    public void testDecodeBooleanFalse() {
        JsonPrimitive primitive = new JsonPrimitive(false);
        BooleanDecoder decoder = new BooleanDecoder();
        Boolean result = decoder.decode(primitive);
        assertEquals(false, result);
    }

    @Test
    public void testDecodeString() {
        JsonPrimitive primitive = new JsonPrimitive("I am a string");
        BooleanDecoder decoder = new BooleanDecoder();
        Boolean result = decoder.decode(primitive);
        assertNull(result);
    }
}
