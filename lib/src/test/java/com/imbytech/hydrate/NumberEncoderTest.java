package com.imbytech.hydrate;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import com.google.gson.JsonElement;

public class NumberEncoderTest {
    @Test
    public void testEncodeNumber() {
        NumberEncoder encoder = new NumberEncoder();
        JsonElement result = encoder.encode(123456);
        assertTrue(result.isJsonPrimitive());
        assertEquals(123456, result.getAsNumber());
    }

    @Test
    public void testEncodeNull() {
        NumberEncoder encoder = new NumberEncoder();
        JsonElement result = encoder.encode(null);
        assertTrue(result.isJsonNull());
    }

    @Test
    public void testEncodeDoubleNaN() {
        NumberEncoder encoder = new NumberEncoder();
        JsonElement result = encoder.encode(Double.NaN);
        assertTrue(result.isJsonNull());
    }

    @Test
    public void testEncodeFloatNaN() {
        NumberEncoder encoder = new NumberEncoder();
        JsonElement result = encoder.encode(Float.NaN);
        assertTrue(result.isJsonNull());
    }
}
