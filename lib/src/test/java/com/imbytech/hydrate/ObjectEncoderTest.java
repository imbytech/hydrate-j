package com.imbytech.hydrate;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import com.google.gson.JsonObject;

public class ObjectEncoderTest {
    @Test
    public void testEncode() {
        Dummy obj = new Dummy("One", 2);
        ObjectEncoder<Dummy> encoder = new ObjectEncoder<Dummy>()
            .withField("one", o -> o.getOne(), new StringEncoder())
            .withField("two", o -> o.getTwo(), new NumberEncoder());
        JsonObject result = encoder.encodeToObject(obj);
        assertEquals("One", result.get("one").getAsString());
        assertEquals(2L, result.get("two").getAsNumber());
    }

    public static class Dummy {
        private String one;
        private long two;

        public Dummy(String one, long two) {
            this.one = one;
            this.two = two;
        }

        public String getOne() {
            return one;
        }

        public long getTwo() {
            return two;
        }
    }
}
