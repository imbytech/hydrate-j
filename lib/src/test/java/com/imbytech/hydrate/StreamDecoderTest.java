package com.imbytech.hydrate;

import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;
import com.google.gson.JsonArray;

public class StreamDecoderTest {
    @Test
    public void testDecodeStringStream() {
        JsonArray array = new JsonArray();
        array.add("hello");
        array.add("world");
        StreamDecoder<String> decoder = new StreamDecoder<String>(new StringDecoder());
        Stream<String> result = decoder.decode(array);
        assertThat(
            new String[]{"hello", "world"},
            is(result.toArray())
            );
    }
}
