package com.imbytech.hydrate;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import com.google.gson.JsonElement;

public class BooleanEncoderTest {
    @Test
    public void testEncodeBoolean() {
        BooleanEncoder encoder = new BooleanEncoder();
        JsonElement result = encoder.encode(true);
        assertTrue(result.isJsonPrimitive());
        assertEquals(true, result.getAsBoolean());
    }

    @Test
    public void testEncodeNull() {
        BooleanEncoder encoder = new BooleanEncoder();
        JsonElement result = encoder.encode(null);
        assertTrue(result.isJsonNull());
    }
}
