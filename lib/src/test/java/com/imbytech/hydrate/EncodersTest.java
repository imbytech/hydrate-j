package com.imbytech.hydrate;

import java.util.Collection;
import java.util.List;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import com.google.gson.JsonElement;

public class EncodersTest {

    // Collection

    @Test
    public void testCollection() {
        List<String> input = new ArrayList<String>();
        input.add("hello");
        input.add("world");
        Encoder<Collection<String>> encoder = Encoders.collectionEncoder(new StringEncoder());
        JsonElement result = encoder.encode(input);
        assertTrue(result.isJsonArray());
        assertEquals(2, result.getAsJsonArray().size());
        assertEquals("hello", result.getAsJsonArray().get(0).getAsString());
        assertEquals("world", result.getAsJsonArray().get(1).getAsString());
    }
}
