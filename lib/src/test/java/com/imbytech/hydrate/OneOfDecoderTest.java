package com.imbytech.hydrate;

import java.time.ZonedDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import com.google.gson.JsonPrimitive;
import com.imbytech.hydrate.time.TimeDecoders;
import com.imbytech.hydrate.time.InstantPrecision;

public class OneOfDecoderTest {
    @Test
    public void testDecodeInstant() {
        ZoneId tz = ZoneId.of("Europe/London");
        JsonPrimitive primitive = new JsonPrimitive(1525129200000L);
        Decoder<ZonedDateTime> decoder = new OneOfDecoder<ZonedDateTime>(
            TimeDecoders.instantDecoder(InstantPrecision.MILLISECONDS).map(v -> ZonedDateTime.ofInstant(v, tz)),
            TimeDecoders.localDateDecoder().map(v -> ZonedDateTime.of(v.atStartOfDay(), tz)),
            TimeDecoders.localDateTimeDecoder().map(v -> ZonedDateTime.of(v, tz)),
            TimeDecoders.zonedDateTimeDecoder()
            );
        ZonedDateTime result = decoder.decode(primitive);
        assertEquals(2018, result.getYear());
        assertEquals(5, result.getMonthValue());
        assertEquals(1, result.getDayOfMonth());
        assertEquals(0, result.getHour());
        assertEquals(0, result.getMinute());
        assertEquals(ZoneOffset.ofHours(1), result.getOffset());
    }

    @Test
    public void testDecodeLocalDate() {
        ZoneId tz = ZoneId.of("Europe/London");
        JsonPrimitive primitive = new JsonPrimitive("2018-05-01");
        Decoder<ZonedDateTime> decoder = new OneOfDecoder<ZonedDateTime>(
            TimeDecoders.instantDecoder(InstantPrecision.MILLISECONDS).map(v -> ZonedDateTime.ofInstant(v, tz)),
            TimeDecoders.localDateDecoder().map(v -> ZonedDateTime.of(v.atStartOfDay(), tz)),
            TimeDecoders.localDateTimeDecoder().map(v -> ZonedDateTime.of(v, tz)),
            TimeDecoders.zonedDateTimeDecoder()
            );
        ZonedDateTime result = decoder.decode(primitive);
        assertEquals(2018, result.getYear());
        assertEquals(5, result.getMonthValue());
        assertEquals(1, result.getDayOfMonth());
        assertEquals(0, result.getHour());
        assertEquals(0, result.getMinute());
        assertEquals(ZoneOffset.ofHours(1), result.getOffset());
    }

    @Test
    public void testDecodeLocalDateTime() {
        ZoneId tz = ZoneId.of("Europe/London");
        JsonPrimitive primitive = new JsonPrimitive("2018-05-01T10:30");
        Decoder<ZonedDateTime> decoder = new OneOfDecoder<ZonedDateTime>(
            TimeDecoders.instantDecoder(InstantPrecision.MILLISECONDS).map(v -> ZonedDateTime.ofInstant(v, tz)),
            TimeDecoders.localDateDecoder().map(v -> ZonedDateTime.of(v.atStartOfDay(), tz)),
            TimeDecoders.localDateTimeDecoder().map(v -> ZonedDateTime.of(v, tz)),
            TimeDecoders.zonedDateTimeDecoder()
            );
        ZonedDateTime result = decoder.decode(primitive);
        assertEquals(2018, result.getYear());
        assertEquals(5, result.getMonthValue());
        assertEquals(1, result.getDayOfMonth());
        assertEquals(10, result.getHour());
        assertEquals(30, result.getMinute());
        assertEquals(ZoneOffset.ofHours(1), result.getOffset());
    }

    @Test
    public void testDecodeZonedDateTime() {
        ZoneId tz = ZoneId.of("Europe/London");
        JsonPrimitive primitive = new JsonPrimitive("2018-05-01T10:30+01:00");
        Decoder<ZonedDateTime> decoder = new OneOfDecoder<ZonedDateTime>(
            TimeDecoders.instantDecoder(InstantPrecision.MILLISECONDS).map(v -> ZonedDateTime.ofInstant(v, tz)),
            TimeDecoders.localDateDecoder().map(v -> ZonedDateTime.of(v.atStartOfDay(), tz)),
            TimeDecoders.localDateTimeDecoder().map(v -> ZonedDateTime.of(v, tz)),
            TimeDecoders.zonedDateTimeDecoder()
            );
        ZonedDateTime result = decoder.decode(primitive);
        assertEquals(2018, result.getYear());
        assertEquals(5, result.getMonthValue());
        assertEquals(1, result.getDayOfMonth());
        assertEquals(10, result.getHour());
        assertEquals(30, result.getMinute());
        assertEquals(ZoneOffset.ofHours(1), result.getOffset());
    }
}
