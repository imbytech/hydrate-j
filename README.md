# Hydrate Java

## Name
Hydrate, Java Edition

## Description
Hydrate is a small library that transforms a loosely typed element coming out of a JSON parser into a strongly typed object. This idea came about when working with the Google Gson library, after having experienced the Elm Decoder / Encoder library. While Gson gives you a loosely typed `JsonElement` that you still have to validate and transform into a Java object, Elm can decode to a strongly typed object and perform all the necessary validation on the way. We wanted to reproduce the same experience in Java while leveraging existing JSON parsers.

The inspiration for the name comes from [The Three-Body Problem](https://www.goodreads.com/book/show/20518872-the-three-body-problem) novel by Liu Cixin. The library hydrates (decodes) a dry, easily transportable `JsonElement` into a fully alive and validated Java object, and dehydrates (encodes) it back into a `JsonElement`.


## Installation
At some point, we will publish it to Maven so that it can be installed by others. We are not quite there yet.

## Usage
Examples in progress.

## Support
If you need help, please open an issue in the [issue tracker](https://gitlab.com/imbytech/hydrate-j/-/issues).

## Roadmap
- [x] Extract the code out of the larger project it was originally in
- [ ] Implement automated CI build and tests
- [ ] Implement semver version bump
- [ ] Refactor to simplify the different packages and isolate the Gson dependent code
- [ ] Add support for YAML
- [ ] Create a [SPI](https://docs.oracle.com/javase/tutorial/ext/basics/spi.html) to integrate low level parsers as plug-ins

## Contributing
Steps to contribute:
1. Open an issue in the [issue tracker](https://gitlab.com/imbytech/hydrate-j/-/issues),
2. If you have the technical know-how, implement the change and send a merge request.

This project is configured as a standard Gradle library project. All code is in the `lib/src` directory. You can run tests via Gradle:

```bash
./gradlew test
```

## Authors and acknowledgment
Created by [imbyTech](https://www.imbytech.com/), as part of a larger project.

## License
This project is licensed under the [Apache 2.0 license](./LICENSE).

## Project status
September 2021: this project is actively maintained by [imbyTech](https://www.imbytech.com/).
